#[cfg(feature = "form_multipart")]
use axum_typed_multipart::TryFromMultipart;
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Query {}

#[derive(Debug, Deserialize)]
#[cfg_attr(feature = "form_multipart", derive(TryFromMultipart))]
pub struct Body {}
