use serde::{de::DeserializeOwned, Serialize};

pub mod user;

pub trait Entity: Send + Unpin + Sync + Serialize + DeserializeOwned + Clone {
    fn id(self) -> String;
}
