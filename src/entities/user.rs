use serde::{Deserialize, Serialize};

use super::Entity;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct User {
    pub id: String,
}

impl Entity for User {
    fn id(self) -> String {
        self.id
    }
}
