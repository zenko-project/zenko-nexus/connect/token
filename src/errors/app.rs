use thiserror::Error;

#[derive(Debug, Error)]
pub enum AppError {
    #[error("Invalid config")]
    InvalidConfig,

    #[error("Invalid host")]
    InvalidHost,

    #[error("Fail to init tracing")]
    FailToInitTracing,

    #[error("App Crashed")]
    Crashed,

    #[error("Failed to initialize redis client")]
    FailedToInitRedisClient,

    #[error("Failed to initialize mongo client")]
    FailedToInitMongoClient,

    #[error("Failed to initialize state")]
    FailedToInitState,
}
