use axum::{
    extract::{Query, State},
    Json,
};
use std::sync::Arc;
use tracing::debug;

use crate::{app::state::AppState, dtos};

pub async fn handle(
    State(state): State<Arc<AppState>>,
    Query(query): Query<dtos::index::Query>,
    Json(body): Json<dtos::index::Body>,
) -> Result<(), ()> {
    debug!("Query: {:?}", query);
    debug!("Body: {:?}", body);

    Ok(())
}
