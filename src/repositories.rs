#[cfg(feature = "mongo")]
pub mod mongo;

use axum::async_trait;

use crate::entities::Entity;

pub trait Record {
    fn id(self) -> String;
}

#[async_trait]
pub trait Repository<E: Entity, C> {
    type Config;
    type Record: Record;
    type SearchFilters;

    async fn get_by_id(&self, id: String) -> Option<E>;
    async fn search(&self, filters: Self::SearchFilters) -> Vec<E>;
    async fn create(&self, entity: E) -> Option<Self::Record>;
    async fn update(&self, entity: E) -> Option<Self::Record>;
    async fn delete(&self, id: String) -> Option<Self::Record>;
}
