pub mod index;

use std::sync::Arc;

use axum::Router;
use tower_http::trace::{DefaultMakeSpan, DefaultOnResponse, TraceLayer};

use crate::app::state::AppState;

pub fn create_routes(state: Arc<AppState>) -> Router {
    Router::new().merge(index::create_routes(state)).layer(
        TraceLayer::new_for_http()
            .make_span_with(DefaultMakeSpan::default())
            .on_response(DefaultOnResponse::new()),
    )
}
