use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct MongoSetting {
    pub uri: String,
    pub database: String,
    pub user_collection: String,
}
